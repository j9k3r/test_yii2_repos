<?php
namespace app\controllers;


use app\models\Guestbook;
use Yii;
use yii\web\Controller;
use app\models\GbForm;

class GbController extends Controller
{

    public function posts()
    {
        $posts = Guestbook::find()->asArray()->all();
            switch ($_GET[sort]){
                case 'asort':
                    krsort($posts);
                    break;
                case 'bsort':
                    shuffle($posts);
                    break;
                case 'ksort':
                    ksort($posts);
                    break;
            }



        return $posts;
    }


    public function actionShow()
    {
        $all = $this->posts();

        $model = new Guestbook(); // создаем объект модели

        if ($model->load(Yii::$app->request->post())) { // && $model->validate() - избыточно
            $model->date = date("Y-m-d H:i:s");
            $model->save(); // валидация тут
            return $this->refresh();

        }

        return $this->render('show', compact('model', 'all')); //передаем объект
    }


}