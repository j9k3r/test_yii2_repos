<?php
namespace app\models;

use yii\db\ActiveRecord;

class Guestbook extends ActiveRecord {

    public static function tableName()
    {
        return 'Guestbooks';
    }

    public function attributeLabels() // переим. заголовков через массив
    {
        return [
            'title' =>  'Заголовок',
            'text' => 'Текст Вашего сообщения',
        ];
    }

    public function rules() //валидация на клиенте
    {
        return [
            //[['title', 'text'], 'safe'], // игнор проверок, только для теста
            [['title', 'text'], 'required', 'message' => 'Заполните данное поле'], //обязательны к заполнению
            [['title', 'text'], 'trim'], // обрезка пробелов по краям
            ['title', 'string', 'max' => 15, 'tooLong' => 'Поле должно содержать не более 15 символов'], // не более 15 символов
            ['text', 'string', 'max' => 60, 'tooLong' => 'Поле должно содержать не более 60 символов'],
            [['title', 'text'], 'string', 'min' => 5, 'tooShort' => 'Поле должно содержать не менее 5 символов'], // не менее 3 символов
            [['title', 'text'], 'myRules'],
        ];
    }
    public function myRules($parametrs) {       // пользовательская проверка на сервере
        if (mb_strlen($this->$parametrs) <= 5){ // проверка полей (меньше или ровно 5 символам)
            $this->addError($parametrs, 'Ошибка');
        }



    }
}