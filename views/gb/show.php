<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<h1>Show Guest Book</h1>

<?php
$options = ['style' =>[
                'border' => '1px solid #ccc',
                'padding' => '10px',
                'margin-bottom' => '20px',]];

?>

<?php $form = ActiveForm::begin() ?>
<?= $form->field($model, 'title')?>
<?= $form->field($model, 'text')->textarea(['rows' => 4]) ?>
<?= Html::submitButton('Отправить') ?>
<?php ActiveForm::end() ?>

<hr>
    <label>Сортировка:</label>
    <select onchange="top.location=this.value">
        <option checked <?php if($_GET['sort'] == 'dsort') echo 'selected'; ?> value="<?= Url::toRoute(['gb/show', 'sort' => 'dsort']) ?>">По порядку</option>
        <option <?php if($_GET['sort'] == 'asort') echo 'selected'; ?> value="<?= Url::toRoute(['gb/show', 'sort' => 'asort']) ?>">В обратном</option>
        <option <?php if($_GET['sort'] == 'bsort') echo 'selected'; ?> value="<?= Url::toRoute(['gb/show', 'sort' => 'bsort']) ?>">Случайная</option>
    </select>


<?php foreach ($all as $showall) {

    $a = Html::tag('p', html::encode($showall[title]) . ' | Дата:' . html::encode($showall[date]));
    $b = Html::tag('div', nl2br(html::encode($showall[text])));
    echo Html::tag('div', $a . $b, $options);
} ?>

